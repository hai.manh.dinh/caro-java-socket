/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ptit.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Cuong Pham
 */
public class FindMatch implements Serializable{
    private String id;
    private List<Player> players;

    public FindMatch() {
    }

    public FindMatch(String id, List<Player> players) {
        this.id = id;
        this.players = players;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

}
